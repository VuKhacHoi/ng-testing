import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { map } from "rxjs/operators";
@Injectable({
  providedIn: 'root'
})
export class UserService {

  constructor( private httpClient : HttpClient) { }

  getData(){
    return this.httpClient.get('api/users').pipe(
      map((value: any) => value)
    )
  }

}
