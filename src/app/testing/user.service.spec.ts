import { TestBed, inject, fakeAsync, tick } from '@angular/core/testing';
import { HttpClientTestingModule, HttpTestingController } from '@angular/common/http/testing';
import { UserService } from './user.service';
import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { HttpClient, HttpErrorResponse } from '@angular/common/http';
import { map } from 'rxjs/operators';
import { stringify } from 'querystring';
import { of } from 'rxjs';

describe('UserService', () => {
  let httpClientSpy: { get: jasmine.Spy };
  let userService: UserService;
  let path: string;
  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [
        HttpClientTestingModule
      ],
      providers: [
        // HttpHandler, HttpClient
      ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA]
    })
    httpClientSpy = jasmine.createSpyObj('HttpClient', ['get']);
    userService = new UserService(<any>httpClientSpy);
  });

  it('should be created', () => {
    const service: UserService = TestBed.get(UserService);
    expect(service).toBeTruthy();
  });

  it('should return expected heroes (HttpClient called three)', () => {
    const expectedHeroes: any[] =
      [
        {
          "name": "haha",
          "age": "10000"
        }
      ];
    let a;
    httpClientSpy.get.and.returnValue(of(expectedHeroes));
    userService.getData().subscribe(
      heroes => expect(heroes).toEqual(expectedHeroes), fail
    );
    userService.getData().subscribe(
      heroes => expect(heroes).toEqual(expectedHeroes), fail
    );
    userService.getData().subscribe(
      heroes => expect(heroes).toEqual(expectedHeroes), fail
    );
    // console.log(httpClientSpy.get.calls.first())
    expect(httpClientSpy.get.calls.count()).toBe(3);
  });

  it('should return expected heroes (HttpClient called once)', (done: DoneFn) => {
    const expectedHeroes: any[] =
      [
        {
          "name": "hahaa",
          "age": "10000"
        }
      ];
    let a;
    httpClientSpy.get.and.returnValue(of(expectedHeroes));
    userService.getData().subscribe(
      heroes => {
        expect(heroes).toEqual(expectedHeroes);
        done();
      }, fail
    );
    expect(httpClientSpy.get.calls.count()).toBe(1);
  });

  it('should return an error when the server returns a 404', () => {
    const errorResponse = new HttpErrorResponse({
      error: 'test 404 error',
      status: 404, statusText: 'Not Found'
    });
    httpClientSpy.get.and.returnValue(of(errorResponse));
    userService.getData().subscribe(
      heroes => fail('expected an error, not heroes'),
      error => expect(error.message).toContain('test 404 error')
    );
  });
<<<<<<< HEAD

  it('should return an error when the server returns a 404 yea yeah  yeaah ', () => {
    const errorResponse = new HttpErrorResponse({
      error: 'test 404 error',
      status: 404, statusText: 'Not Found'
    });

    httpClientSpy.get.and.returnValue(of(errorResponse));

    userService.getData().subscribe(
      heroes => fail('expected an error, not heroes'),
      error => expect(error.message).toContain('test 404 error')
    );
  });

=======
>>>>>>> 59d112fe18a4fd9aa9be79d340d834880c433d20
});
