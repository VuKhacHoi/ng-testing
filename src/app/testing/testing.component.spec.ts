import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TestingComponent } from './testing.component';
import { By } from '@angular/platform-browser';
import { HttpClient, HttpHandler } from '@angular/common/http';
import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { HttpTestingController, HttpClientTestingModule } from '@angular/common/http/testing';
import { of } from 'rxjs';

let component: TestingComponent;
let fixture: ComponentFixture<TestingComponent>;
let httpController: HttpTestingController;
describe('TestingComponent', () => {

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [TestingComponent],
      imports: [HttpClientTestingModule],
      schemas: [CUSTOM_ELEMENTS_SCHEMA]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TestingComponent);
    component = fixture.componentInstance;
    httpController = TestBed.get(HttpTestingController);
    fixture.detectChanges();
  });



  it('should create', () => {
    expect(component).toBeTruthy();
  });
  xit('should call api to URL', () => {
    const mockResult = [
      {
      "name": "haha",
      "age": "10000"
      }
      ];
    const expectedValue = [
      {
      "name": "haha",
      "age": "10000"
      }
      ];
   let a= component.getData()
    console.log(a)
    // Method 1
    const req = httpController.expectOne('/api/users');
    expect(req.request.method).toEqual('GET');

    // Method 2
    // const req = httpController.expectOne({
    //   method: 'GET',
    //   url: '/api/users'
    // });

    // req.flush(mockResult); // Dòng này sẽ làm cho HTTP chính thức được phát hành.

    httpController.verify();
  });

  // it('use spyOn', () => {
  //   const mockResult = [
  //     {
  //     "name": "haha",
  //     "age": "10000"
  //     }
  //     ];
  //   const expectedValue = [
  //     {
  //     "name": "haha",
  //     "age": "10000"
  //     }
  //     ];

  //   spyOn(component['http'], 'get').and.returnValue(of(mockResult));

  //   component.getData().subscribe(value => {
  //     expect(component['http'].get).toHaveBeenCalledWith('/api/users');
  //     expect(value).toEqual(expectedValue);
  //   })
  // });
  it('message should be ON', () => {
    component.clicked();
    expect(component.isOn).toBeTruthy();
    expect(component.message).toBe('ON');
  });

  it('message should be OFF', () => {
    component.isOn = true;
    component.clicked();
    expect(component.isOn).toBeFalsy();
    expect(component.message).toBe('OFF');
  });

  it('display should be ON', () => {
    let nativeElement = fixture.debugElement.nativeElement as HTMLElement;
    let spanElement = nativeElement.querySelector('span') as HTMLSpanElement;
    component.clicked();
    fixture.detectChanges();
    expect(spanElement.innerHTML).toContain('ON');
  });

  it('display should be OFF', () => {
    let nativeElement = fixture.debugElement.nativeElement as HTMLElement;
    let spanElement = nativeElement.querySelector('span') as HTMLSpanElement;
    component.isOn = true;
    fixture.detectChanges();
    expect(spanElement.innerHTML).toContain('ON');

    component.clicked();
    fixture.detectChanges();
    expect(spanElement.innerHTML).toContain('OFF');
  });


  it('display should be ON using debugElement', () => {

    var de = fixture.debugElement.query(By.css('#btn'));
    de.triggerEventHandler('click', {});

    fixture.detectChanges();

    var span = fixture.debugElement.query(By.css('span'));

    expect(span.nativeElement.innerHTML).toContain('ON');
  });

  it('display should be ON and allow component to async operation', () => {
    let nativeElement = fixture.debugElement.nativeElement as HTMLElement;
    let spanElement = nativeElement.querySelector('span') as HTMLSpanElement;
    component.clicked();
    fixture.detectChanges();
    fixture.whenStable().then(() => {
      expect(spanElement.innerHTML).toContain('ON');
    })
  });

  

});



