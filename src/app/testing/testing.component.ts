import { Component, OnInit } from '@angular/core';
import { UserService } from './user.service';
import { map } from 'rxjs/operators';
import { HttpClient } from '@angular/common/http';

@Component({
  selector: 'app-testing',
  templateUrl: './testing.component.html',
  styleUrls: ['./testing.component.scss']
})
export class TestingComponent implements OnInit {

  constructor(private httpClient : HttpClient,private userService:UserService) { }
  ngOnInit() {
    console.log('ngOnInit called');
    this.callAPI();
  }

  isOn: boolean;

  clicked() {
    this.isOn = !this.isOn;
  }
  data: { name: string, age: string };
  get message() {
    return this.isOn ? 'ON' : 'OFF';
  }

  callAPI() {
    this.userService.getData().subscribe(value => {
      this.data = value;
      console.log(value)
    })
  }
  getData(){
    return this.httpClient.get('api/users').pipe(
      map((value: any) => value)
    )
  }
}
